package com.example.spark;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Avvenimento implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6584007070153763389L;
	
	private long id;
	private long idavvproviderlive;
	private long idbetradar;
	private long iddisc;
	private long idmanif;
	private long idproviderlive;
	private long legmultipla;
	private long live;
	private long stato;
	
	//TODO: creare classe Classe
	private List<Classe> classi = new ArrayList<Classe>();
	private String data;
	private String descrdisc;
	private String descrizione;
	private String descrmanif;
	private String digladisc;
	private String siglamanif;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getIdavvproviderlive() {
		return idavvproviderlive;
	}
	public void setIdavvproviderlive(long idavvproviderlive) {
		this.idavvproviderlive = idavvproviderlive;
	}
	public long getIdbetradar() {
		return idbetradar;
	}
	public void setIdbetradar(long idbetradar) {
		this.idbetradar = idbetradar;
	}
	public long getIddisc() {
		return iddisc;
	}
	public void setIddisc(long iddisc) {
		this.iddisc = iddisc;
	}
	public long getIdmanif() {
		return idmanif;
	}
	public void setIdmanif(long idmanif) {
		this.idmanif = idmanif;
	}
	public long getIdproviderlive() {
		return idproviderlive;
	}
	public void setIdproviderlive(long idproviderlive) {
		this.idproviderlive = idproviderlive;
	}
	public long getLegmultipla() {
		return legmultipla;
	}
	public void setLegmultipla(long legmultipla) {
		this.legmultipla = legmultipla;
	}
	public long getLive() {
		return live;
	}
	public void setLive(long live) {
		this.live = live;
	}
	public long getStato() {
		return stato;
	}
	public void setStato(long stato) {
		this.stato = stato;
	}
	public List<Classe> getClassi() {
		return classi;
	}
	public void setClassi(List<Classe> classi) {
		this.classi = classi;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	public String getDescrdisc() {
		return descrdisc;
	}
	public void setDescrdisc(String descrdisc) {
		this.descrdisc = descrdisc;
	}
	public String getDescrizione() {
		return descrizione;
	}
	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}
	public String getDescrmanif() {
		return descrmanif;
	}
	public void setDescrmanif(String descrmanif) {
		this.descrmanif = descrmanif;
	}
	public String getDigladisc() {
		return digladisc;
	}
	public void setDigladisc(String digladisc) {
		this.digladisc = digladisc;
	}
	public String getSiglamanif() {
		return siglamanif;
	}
	public void setSiglamanif(String siglamanif) {
		this.siglamanif = siglamanif;
	}
	
	
}
