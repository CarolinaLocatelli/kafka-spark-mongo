package com.example.spark;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Classe implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4710069559391656670L;
	
	private long _id;
	private long _blacklistmax;
	private long _blacklistmin;
	private String _descrinfoagg;
	private long _infoagg;
	private long _legmax;
	private long _legmin;
	private long _live;
	private long _livedelay;
	private long _multipla;
	private long _stato;
	private long _tipoinfoagg;
	private String descrizione;
	private List<Esito> esiti = new ArrayList<Esito>();
	
	public long get_id() {
		return _id;
	}
	public void set_id(long _id) {
		this._id = _id;
	}
	public long get_blacklistmax() {
		return _blacklistmax;
	}
	public void set_blacklistmax(long _blacklistmax) {
		this._blacklistmax = _blacklistmax;
	}
	public long get_blacklistmin() {
		return _blacklistmin;
	}
	public void set_blacklistmin(long _blacklistmin) {
		this._blacklistmin = _blacklistmin;
	}
	public String get_descrinfoagg() {
		return _descrinfoagg;
	}
	public void set_descrinfoagg(String _descrinfoagg) {
		this._descrinfoagg = _descrinfoagg;
	}
	public long get_infoagg() {
		return _infoagg;
	}
	public void set_infoagg(long _infoagg) {
		this._infoagg = _infoagg;
	}
	public long get_legmax() {
		return _legmax;
	}
	public void set_legmax(long _legmax) {
		this._legmax = _legmax;
	}
	public long get_legmin() {
		return _legmin;
	}
	public void set_legmin(long _legmin) {
		this._legmin = _legmin;
	}
	public long get_live() {
		return _live;
	}
	public void set_live(long _live) {
		this._live = _live;
	}
	public long get_livedelay() {
		return _livedelay;
	}
	public void set_livedelay(long _livedelay) {
		this._livedelay = _livedelay;
	}
	public long get_multipla() {
		return _multipla;
	}
	public void set_multipla(long _multipla) {
		this._multipla = _multipla;
	}
	public long get_stato() {
		return _stato;
	}
	public void set_stato(long _stato) {
		this._stato = _stato;
	}
	public long get_tipoinfoagg() {
		return _tipoinfoagg;
	}
	public void set_tipoinfoagg(long _tipoinfoagg) {
		this._tipoinfoagg = _tipoinfoagg;
	}
	public String getDescrizione() {
		return descrizione;
	}
	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}
	public List<Esito> getEsiti() {
		return esiti;
	}
	public void setEsiti(List<Esito> esiti) {
		this.esiti = esiti;
	}
	
	
	
	

}
