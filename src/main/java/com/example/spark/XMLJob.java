package com.example.spark;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.SparkConf;
import org.apache.spark.SparkContext;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SQLContext;
import org.apache.spark.sql.SparkSession;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.io.Serializable;

import org.apache.spark.api.java.function.MapFunction;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.Encoder;
import org.apache.spark.sql.Encoders;

public final class XMLJob {

    private XMLJob() {
    }
    
    public static void main(String[] args) throws Exception {
    	
    	setLogLevels();
    	SparkConf conf = new SparkConf().setAppName("XMLJob").setMaster("local");
    	SparkContext sc = new SparkContext(conf);
    	
    	SparkSession spark = SparkSession
    			  .builder()
    			  .appName("Java Spark SQL basic example")
    			  .config("spark.mongodb.output.uri", "mongodb://127.0.0.1/test.myCollection")
    			  .master("local[2]")
    			  .getOrCreate();
    	
    	Dataset<Row> df = spark.read()
    		    .format("com.databricks.spark.xml")
    		    .option("rowTag", "palinsesti")
    		    .load("palinsesti_message.xml");
    		    //.option("rowTag", "book")
    		    //.load("books.xml");
    	
    	
    	// Displays the content of the DataFrame to stdout
    	df.show();
    	
    	// Print the schema in a tree format
    	df.printSchema();
    	
    	// Select only the "name" column and print the number of books of each author
    	//df.select("author").groupBy("author").count().show();
    	
    	//____________________________________
    	// Encoders are created for Java beans
    	//Encoder<Book> bookEncoder = Encoders.bean(Book.class);
    	
    	MessaggioPalinsesti mp = new MessaggioPalinsesti();
    	Palinsesto p = new Palinsesto();
    	
    	
    	ArrayList<Palinsesto> ar = new ArrayList<Palinsesto>();
        ar.add(p);         
        mp.setPalinsesto(ar); 
    	
    	
    	ArrayList<Avvenimento> avList = new ArrayList<Avvenimento>();
    	Avvenimento a = new Avvenimento();
    	
    	// DataFrames can be converted to a Dataset by providing a class. Mapping based on name
    	//Dataset<Book> booksDS = df.as(bookEncoder);
    	//booksDS.show();
    	//Dataset<MessaggioPalinsesti> messaggiDS = df.as(messaggioEncoder);
    	Encoder<MessaggioPalinsesti> messaggioEncoder = Encoders.bean(MessaggioPalinsesti.class);
    	//Dataset<MessaggioPalinsesti> messaggiDS = spark.createDataset(Collections.singletonList(mp), messaggioEncoder);
    	Dataset<MessaggioPalinsesti> messaggiDS = df.as(messaggioEncoder);
    	messaggiDS.show();
    	
    	messaggiDS.createOrReplaceTempView("messaggio_palinsesti");
    	//spark.sql("SELECT COUNT(*) FROM messaggio_palinsesti.palinsesto").show();
    	Dataset<Row> avvenimenti = df.select("palinsesto.avvenimenti");
    	avvenimenti.show();
    	avvenimenti.selectExpr("explode(avvenimenti) as avv").show();
    	avvenimenti.selectExpr("explode(avvenimenti) as avv").selectExpr("avv.avvenimento.descrizione").show();
    	
    }
    private static void setLogLevels() {
        boolean log4jInitialized = Logger.getRootLogger().getAllAppenders().hasMoreElements();
        if (!log4jInitialized) {
            // We first log something to initialize Spark's default logging, then we override the
            // logging level.
            Logger.getLogger(XMLJob.class).info("Setting log level to [WARN] for streaming example." +
                    " To override add a custom log4j.properties to the classpath.");
            Logger.getRootLogger().setLevel(Level.WARN);
        }
    }

}
