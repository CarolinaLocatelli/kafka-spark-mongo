package com.example.spark;

import java.io.Serializable;

public class ClassiAvvenimentoEsito implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2328897777692688384L;
	
	private long idAvvenimento;
	private String dataAvvenimento;
	private String descrmanifAvvenimento;
	private String descrizioneAvvenimento;
	private long idClasse;
	private String descrinfoaggClasse;
	private long idEsito;
	private long quotaEsito;
	private String descrizioneEsito;
	
	
	public Long getIdAvvenimento() {
		return idAvvenimento;
	}
	public void setIdAvvenimento(Long idAvvenimento) {
		this.idAvvenimento = idAvvenimento;
	}
	public String getDataAvvenimento() {
		return dataAvvenimento;
	}
	public void setDataAvvenimento(String dataAvvenimento) {
		this.dataAvvenimento = dataAvvenimento;
	}
	public String getDescrmanifAvvenimento() {
		return descrmanifAvvenimento;
	}
	public void setDescrmanifAvvenimento(String descrmanifAvvenimento) {
		this.descrmanifAvvenimento = descrmanifAvvenimento;
	}
	public String getDescrizioneAvvenimento() {
		return descrizioneAvvenimento;
	}
	public void setDescrizioneAvvenimento(String descrizioneAvvenimento) {
		this.descrizioneAvvenimento = descrizioneAvvenimento;
	}
	public Long getIdClasse() {
		return idClasse;
	}
	public void setIdClasse(Long idClasse) {
		this.idClasse = idClasse;
	}
	public String getDescrinfoaggClasse() {
		return descrinfoaggClasse;
	}
	public void setDescrinfoaggClasse(String descrinfoaggClasse) {
		this.descrinfoaggClasse = descrinfoaggClasse;
	}
	public long getIdEsito() {
		return idEsito;
	}
	public void setIdEsito(long idEsito) {
		this.idEsito = idEsito;
	}
	public long getQuotaEsito() {
		return quotaEsito;
	}
	public void setQuotaEsito(long quotaEsito) {
		this.quotaEsito = quotaEsito;
	}
	public String getDescrizioneEsito() {
		return descrizioneEsito;
	}
	public void setDescrizioneEsito(String descrizioneEsito) {
		this.descrizioneEsito = descrizioneEsito;
	}
	public void setIdAvvenimento(long idAvvenimento) {
		this.idAvvenimento = idAvvenimento;
	}
	public void setIdClasse(long idClasse) {
		this.idClasse = idClasse;
	}
	
	
	

}
