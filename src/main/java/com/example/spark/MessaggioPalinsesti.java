package com.example.spark;

import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class MessaggioPalinsesti implements Serializable {
	
	
	long _type;
	long bmnumminesiti;
	long bmpercentuale;
	long bmquotaminesiti;
	long bmssnumminesiti;
	long bmsspercentuale;
	long bmssquotaminesiti;
	
	//TODO: creare classe palinsesto
	ArrayList<Palinsesto> palinsesto = new ArrayList<Palinsesto>();
	
	long scomimpmin;
	long scommoltip;
	long scomsoglialeg;
	long scomvinmax;
	long sogliaaccettazione;
	long sscomclsdisab;
	long sscomimpminbigl;
	long sscomimpminunita;
	long sscommaxnumesiti;
	long sscommingrp;
	long sscommoltip;
	long sscomscadavv;
	
	public MessaggioPalinsesti() {};
	
	public long get_type() {
		return _type;
	}
	public void set_type(long _type) {
		this._type = _type;
	}
	public long getBmnumminesiti() {
		return bmnumminesiti;
	}
	public void setBmnumminesiti(long bmnumminesiti) {
		this.bmnumminesiti = bmnumminesiti;
	}
	public long getBmpercentuale() {
		return bmpercentuale;
	}
	public void setBmpercentuale(long bmpercentuale) {
		this.bmpercentuale = bmpercentuale;
	}
	public long getBmquotaminesiti() {
		return bmquotaminesiti;
	}
	public void setBmquotaminesiti(long bmquotaminesiti) {
		this.bmquotaminesiti = bmquotaminesiti;
	}
	public long getBmssnumminesiti() {
		return bmssnumminesiti;
	}
	public void setBmssnumminesiti(long bmssnumminesiti) {
		this.bmssnumminesiti = bmssnumminesiti;
	}
	public long getBmsspercentuale() {
		return bmsspercentuale;
	}
	public void setBmsspercentuale(long bmsspercentuale) {
		this.bmsspercentuale = bmsspercentuale;
	}
	public long getBmssquotaminesiti() {
		return bmssquotaminesiti;
	}
	public void setBmssquotaminesiti(long bmssquotaminesiti) {
		this.bmssquotaminesiti = bmssquotaminesiti;
	}
	public ArrayList<Palinsesto> getPalinsesto() {
		return palinsesto;
	}
	public void setPalinsesto(ArrayList<Palinsesto> palinsesto) {
		this.palinsesto = palinsesto;
	}
	public long getScomimpmin() {
		return scomimpmin;
	}
	public void setScomimpmin(long scomimpmin) {
		this.scomimpmin = scomimpmin;
	}
	public long getScommoltip() {
		return scommoltip;
	}
	public void setScommoltip(long scommoltip) {
		this.scommoltip = scommoltip;
	}
	public long getScomsoglialeg() {
		return scomsoglialeg;
	}
	public void setScomsoglialeg(long scomsoglialeg) {
		this.scomsoglialeg = scomsoglialeg;
	}
	public long getScomvinmax() {
		return scomvinmax;
	}
	public void setScomvinmax(long scomvinmax) {
		this.scomvinmax = scomvinmax;
	}
	public long getSogliaaccettazione() {
		return sogliaaccettazione;
	}
	public void setSogliaaccettazione(long sogliaaccettazione) {
		this.sogliaaccettazione = sogliaaccettazione;
	}
	public long getSscomclsdisab() {
		return sscomclsdisab;
	}
	public void setSscomclsdisab(long sscomclsdisab) {
		this.sscomclsdisab = sscomclsdisab;
	}
	public long getSscomimpminbigl() {
		return sscomimpminbigl;
	}
	public void setSscomimpminbigl(long sscomimpminbigl) {
		this.sscomimpminbigl = sscomimpminbigl;
	}
	public long getSscomimpminunita() {
		return sscomimpminunita;
	}
	public void setSscomimpminunita(long sscomimpminunita) {
		this.sscomimpminunita = sscomimpminunita;
	}
	public long getSscommaxnumesiti() {
		return sscommaxnumesiti;
	}
	public void setSscommaxnumesiti(long sscommaxnumesiti) {
		this.sscommaxnumesiti = sscommaxnumesiti;
	}
	public long getSscommingrp() {
		return sscommingrp;
	}
	public void setSscommingrp(long sscommingrp) {
		this.sscommingrp = sscommingrp;
	}
	public long getSscommoltip() {
		return sscommoltip;
	}
	public void setSscommoltip(long sscommoltip) {
		this.sscommoltip = sscommoltip;
	}
	public long getSscomscadavv() {
		return sscomscadavv;
	}
	public void setSscomscadavv(long sscomscadavv) {
		this.sscomscadavv = sscomscadavv;
	}
	
	
	
	
}
