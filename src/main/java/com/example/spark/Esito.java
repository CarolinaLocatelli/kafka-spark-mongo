package com.example.spark;

import java.io.Serializable;

public class Esito implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2328897777692688384L;
	
	private long _id;
	private long _quota;
	private long _stato;
	private String descrizione;
	
	
	public long get_id() {
		return _id;
	}
	public void set_id(long _id) {
		this._id = _id;
	}
	public long get_quota() {
		return _quota;
	}
	public void set_quota(long _quota) {
		this._quota = _quota;
	}
	public long get_stato() {
		return _stato;
	}
	public void set_stato(long _stato) {
		this._stato = _stato;
	}
	public String getDescrizione() {
		return descrizione;
	}
	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}
	
	

}
