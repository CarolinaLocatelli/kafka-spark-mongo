package com.example.spark;


import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.SparkConf;
import org.apache.spark.SparkContext;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SQLContext;
import org.apache.spark.sql.SparkSession;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.io.Serializable;

import org.apache.spark.api.java.function.MapFunction;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.Encoder;
import org.apache.spark.sql.Encoders;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.VoidFunction;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import org.apache.spark.streaming.Duration;
import org.apache.spark.streaming.api.java.JavaDStream;
import org.apache.spark.streaming.api.java.JavaPairInputDStream;
import org.apache.spark.streaming.api.java.JavaPairReceiverInputDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import org.apache.spark.streaming.kafka.KafkaUtils;
import org.bson.Document;

import com.databricks.spark.xml.XmlReader;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.spark.MongoSpark;
import com.mongodb.spark.config.ReadConfig;

import kafka.serializer.StringDecoder;
import scala.Tuple2;

public final class XMLJobNew {
	
	/** Constructor */
	private XMLJobNew() {
    }

	/** Main program */
    public static void main(String[] args) throws Exception {   	
    	
    	setLogLevels();

    	Map<String, String> kafkaParams = new HashMap<String, String>();

        kafkaParams.put("zookeeper.connect", "localhost:2181"); //Make all kafka data for this cluster appear under a particular path. 
        kafkaParams.put("group.id", "my-consumer-group");   //String that uniquely identifies the group of consumer processes to which this consumer belongs
        kafkaParams.put("metadata.broker.list", "localhost:9092"); //Producer can find a one or more Brokers to determine the Leader for each topic.
        kafkaParams.put("serializer.class", "kafka.serializer.StringEncoder"); //Serializer to use when preparing the message for transmission to the Broker.
        kafkaParams.put("request.required.acks", "1");  //Producer to require an acknowledgement from the Broker that the message was received.

    	SparkConf sparkConf = new SparkConf().setAppName("StreamingJob")
    			.setMaster("local[4]");
    	
    	 // Create the context with 2 seconds batch size
        JavaStreamingContext jssc = new JavaStreamingContext(sparkConf, new Duration(2000));
        
    	Set<String> topics = Collections.singleton("test");

    	//Creates an input DStream for Receiving data from socket
        JavaPairInputDStream<String, String> directKafkaStream = KafkaUtils.createDirectStream(jssc,
                String.class, 
                String.class, 
                StringDecoder.class, 
                StringDecoder.class, 
                kafkaParams, topics);
        
      //Creates JavaDStream<String>
       JavaDStream<String> msgDataStream = directKafkaStream.map(new Function<Tuple2<String, String>, String>() {
            @Override
            public String call(Tuple2<String, String> tuple2) {
              return tuple2._2();
            }
          });
       
     //Create JavaRDD<Row>
       msgDataStream.foreachRDD(new VoidFunction<JavaRDD<String>>() {
             
    	   	//Cumulative dataset for messages
			//private Dataset<ClassiAvvenimentoEsito> classiavvDS;
    	   	private Dataset<Row> classiavvesitoCumulativoDS;
    	   	private Dataset<Row> quoteDistinteDS;
			//To check if it is the first message (and the dataset is empty)
			private boolean flagPrimoInserimento = true;
			

			@Override
             public void call(JavaRDD<String> rdd) { 
                 JavaRDD<Row> rowRDD = rdd.map(new Function<String, Row>() {
                     @Override
                     public Row call(String msg) {
                       Row row = RowFactory.create(msg);
                       
                       return row;
                     }
                   });
                
                 
                 //Get Spark 2.0 session       
       			SparkSession spark = JavaSparkSessionSingleton.getInstance(rdd.context().getConf());
       			
       			//I need to parse the string as XML (message)
       			Dataset<Row> df = new XmlReader().xmlRdd(spark.sqlContext(), rdd.rdd());
       			
       			       			
       			if(!df.rdd().isEmpty()){	//if the kafka message is not empty...
       				
       				//df.show();
      				// Creates a temporary view using the messages DataFrame
           	        df.createOrReplaceTempView("messaggi");
           	        
           	        
           	        /** MessaggioPalinsesti{Palinsesto{Avvenimento{Classe{Esito}}}}

           	        - create a dataset containing all the message fields
           	        - create a cumulative dataset containing all the message fields
           	        - TODO: check if dataset1 contains a message having an already present idClasse-idEsito-quota: if true, update dataset2 with the new Esito
           	        - group Esiti into the same idClasse and classi into the same idAvvenimento 
           	        TODO: NON PRENDE IL VALORE DI TIMESTAMP MASSIMO **/
           	                   
           	        // Select messages fields
           	        Dataset<Row> classiAvvenimentoEsitoDataFrame = spark.sql("select _type as typeMessaggio"
           	        		+ ", timestamp"
           	        		+ ", palinsesto['_id'] as idPalinsesto"
           	        		+ ", palinsesto.avvenimenti.avvenimento['_id'] as idAvvenimento"
           	        		+ ", palinsesto.avvenimenti.avvenimento['_idavvproviderlive'] as idavvproviderliveAvvenimento"
           	        		+ ", palinsesto.avvenimenti.avvenimento['_idbetradar'] as idbetradarliveAvvenimento"
           	        		+ ", palinsesto.avvenimenti.avvenimento['_iddisc'] as iddiscAvvenimento"
           	        		+ ", palinsesto.avvenimenti.avvenimento['_idmanif'] as idmanifAvvenimento"
           	        		+ ", palinsesto.avvenimenti.avvenimento['_idproviderlive'] as idproviderliveAvvenimento"
           	        		+ ", palinsesto.avvenimenti.avvenimento['_legmultipla'] as legmultiplaAvvenimento"
           	        		+ ", palinsesto.avvenimenti.avvenimento['_live'] as liveAvvenimento"
           	        		+ ", palinsesto.avvenimenti.avvenimento['_stato'] as statoAvvenimento"
           	        		+ ", palinsesto.avvenimenti.avvenimento.data as dataAvvenimento"
           	        		+ ", palinsesto.avvenimenti.avvenimento.descrdisc as descrdiscAvvenimento"
           	        		+ ", palinsesto.avvenimenti.avvenimento.descrmanif as descrmanifAvvenimento"
           	        		+ ", palinsesto.avvenimenti.avvenimento.descrizione as descrizioneAvvenimento"
           	        		+ ", palinsesto.avvenimenti.avvenimento.sigladisc as sigladiscAvvenimento"
           	        		+ ", palinsesto.avvenimenti.avvenimento.siglamanif as siglamanifAvvenimento"
           	        		+ ", palinsesto.avvenimenti.avvenimento.classi.classe['_id'] as idClasse"
           	        		+ ", palinsesto.avvenimenti.avvenimento.classi.classe['_blacklistmax'] as blacklistmaxClasse"
           	        		+ ", palinsesto.avvenimenti.avvenimento.classi.classe['_blacklistmin'] as blacklistminClasse"
           	        		+ ", palinsesto.avvenimenti.avvenimento.classi.classe['_descrinfoagg'] as descrinfoaggClasse"
           	        		+ ", palinsesto.avvenimenti.avvenimento.classi.classe['_infoagg'] as infoaggClasse"
           	        		+ ", palinsesto.avvenimenti.avvenimento.classi.classe['_legmax'] as legmaxClasse"
           	        		+ ", palinsesto.avvenimenti.avvenimento.classi.classe['_legmin'] as legminClasse"
           	        		+ ", palinsesto.avvenimenti.avvenimento.classi.classe['_live'] as liveClasse"
           	        		+ ", palinsesto.avvenimenti.avvenimento.classi.classe['_livedelay'] as livedelayClasse"
           	        		+ ", palinsesto.avvenimenti.avvenimento.classi.classe['_multipla'] as multiplaClasse"
           	        		+ ", palinsesto.avvenimenti.avvenimento.classi.classe['_stato'] as statoClasse"
           	        		+ ", palinsesto.avvenimenti.avvenimento.classi.classe['_tipoinfoagg'] as tipoinfoaggClasse"
           	        		+ ", palinsesto.avvenimenti.avvenimento.classi.classe['descrizione'] as descrizioneClasse"
           	        		+ ", palinsesto.avvenimenti.avvenimento.classi.classe.esiti.esito['_id'] as idEsito"
           	        		+ ", palinsesto.avvenimenti.avvenimento.classi.classe.esiti.esito['_quota'] as quotaEsito"
           	        		+ ", palinsesto.avvenimenti.avvenimento.classi.classe.esiti.esito['_stato'] as statoEsito"
           	        		+ ", palinsesto.avvenimenti.avvenimento.classi.classe.esiti.esito.descrizione as descrizioneEsito"
           	        		+ " from messaggi");
           	        		
           	        if(!classiAvvenimentoEsitoDataFrame.rdd().isEmpty()){        	        	
        	        	if(flagPrimoInserimento) {
        	        		classiavvesitoCumulativoDS = classiAvvenimentoEsitoDataFrame.persist();
        	        		
        	        		flagPrimoInserimento = false;
        	        	} else {
        	        		classiavvesitoCumulativoDS = (classiavvesitoCumulativoDS.union(classiAvvenimentoEsitoDataFrame)).persist();	
        	        		
        	        	}
        	        	
        	        	//se esistono esiti multipli, significa che ho aggiornato la quota quindi considero quella più recente
        	        	classiavvesitoCumulativoDS.createOrReplaceTempView("ultimeQuote");
        	        	
        	        	Dataset<Row> ultimeQuoteDS = spark.sql("select first(typeMessaggio) as typeMessaggio"
        	        			+ ", max(timestamp) as timestamp"
               	        		+ ", idPalinsesto"
               	        		+ ", idAvvenimento"
               	        		+ ", first(idavvproviderliveAvvenimento) as idavvproviderliveAvvenimento"
               	        		+ ", first(idbetradarliveAvvenimento) as idbetradarliveAvvenimento"
               	        		+ ", first(iddiscAvvenimento) as iddiscAvvenimento"
               	        		+ ", first(idmanifAvvenimento) as idmanifAvvenimento"
               	        		+ ", first(idproviderliveAvvenimento) as idproviderliveAvvenimento"
               	        		+ ", first(legmultiplaAvvenimento) as legmultiplaAvvenimento"
               	        		+ ", first(liveAvvenimento) as liveAvvenimento"
               	        		+ ", first(statoAvvenimento) as statoAvvenimento"
               	        		+ ", first(dataAvvenimento) as dataAvvenimento"
               	        		+ ", first(descrdiscAvvenimento) as descrdiscAvvenimento"
               	        		+ ", first(descrmanifAvvenimento) as descrmanifAvvenimento"
               	        		+ ", first(descrizioneAvvenimento) as descrizioneAvvenimento"
               	        		+ ", first(sigladiscAvvenimento) as sigladiscAvvenimento"
               	        		+ ", first(siglamanifAvvenimento) as siglamanifAvvenimento"
               	        		+ ", idClasse"
               	        		+ ", first(blacklistmaxClasse) as blacklistmaxClasse"
               	        		+ ", first(blacklistminClasse) as blacklistminClasse"
               	        		+ ", first(descrinfoaggClasse) as descrinfoaggClasse"
               	        		+ ", first(infoaggClasse) as infoaggClasse"
               	        		+ ", first(legmaxClasse) as legmaxClasse"
               	        		+ ", first(legminClasse) as legminClasse"
               	        		+ ", first(liveClasse) as liveClasse"
               	        		+ ", first(livedelayClasse) as livedelayClasse"
               	        		+ ", first(multiplaClasse) as multiplaClasse"
               	        		+ ", first(statoClasse) as statoClasse"
               	        		+ ", first(tipoinfoaggClasse) as tipoinfoaggClasse"
               	        		+ ", first(descrizioneClasse) as descrizioneClasse"
               	        		+ ", idEsito"
               	        		+ ", first(quotaEsito) as quotaEsito" 
               	        		+ ", first(statoEsito) as statoEsito" 
               	        		+ ", first(descrizioneEsito) as descrizioneEsito" 
        	        			+ " from ultimeQuote"
        	        			+ " group by idPalinsesto, idAvvenimento, idClasse, idEsito"
        	        			
        	        			);
        	        	
        	        	
        	        	
        	        	ultimeQuoteDS.show();	
        	        	
        	       	//classiavvesitoCumulativoDS.createOrReplaceTempView("gruppoEsiti");
        	        	ultimeQuoteDS.createOrReplaceTempView("gruppoEsiti");
               	        Dataset<Row> esitiInClassiDS = spark.sql("select first(typeMessaggio) as typeMessaggio"
               	        		+ ", first(timestamp) as timestamp"
               	        		+ ", idPalinsesto"
               	        		+ ", idAvvenimento"
               	        		+ ", first(idavvproviderliveAvvenimento) as idavvproviderliveAvvenimento"
               	        		+ ", first(idbetradarliveAvvenimento) as idbetradarliveAvvenimento"
               	        		+ ", first(iddiscAvvenimento) as iddiscAvvenimento"
               	        		+ ", first(idmanifAvvenimento) as idmanifAvvenimento"
               	        		+ ", first(idproviderliveAvvenimento) as idproviderliveAvvenimento"
               	        		+ ", first(legmultiplaAvvenimento) as legmultiplaAvvenimento"
               	        		+ ", first(liveAvvenimento) as liveAvvenimento"
               	        		+ ", first(statoAvvenimento) as statoAvvenimento"
               	        		+ ", first(dataAvvenimento) as dataAvvenimento"
               	        		+ ", first(descrdiscAvvenimento) as descrdiscAvvenimento"
               	        		+ ", first(descrmanifAvvenimento) as descrmanifAvvenimento"
               	        		+ ", first(descrizioneAvvenimento) as descrizioneAvvenimento"
               	        		+ ", first(sigladiscAvvenimento) as sigladiscAvvenimento"
               	        		+ ", first(siglamanifAvvenimento) as siglamanifAvvenimento"
               	        		+ ", idClasse"
               	        		+ ", first(blacklistmaxClasse) as blacklistmaxClasse"
               	        		+ ", first(blacklistminClasse) as blacklistminClasse"
               	        		+ ", first(descrinfoaggClasse) as descrinfoaggClasse"
               	        		+ ", first(infoaggClasse) as infoaggClasse"
               	        		+ ", first(legmaxClasse) as legmaxClasse"
               	        		+ ", first(legminClasse) as legminClasse"
               	        		+ ", first(liveClasse) as liveClasse"
               	        		+ ", first(livedelayClasse) as livedelayClasse"
               	        		+ ", first(multiplaClasse) as multiplaClasse"
               	        		+ ", first(statoClasse) as statoClasse"
               	        		+ ", first(tipoinfoaggClasse) as tipoinfoaggClasse"
               	        		+ ", first(descrizioneClasse) as descrizioneClasse"
               	        		+ ", collect_list(struct(idEsito, quotaEsito, statoEsito, descrizioneEsito)) as esiti"
               	        		+ " from gruppoEsiti"
               	        		+ " group by idPalinsesto, idAvvenimento, idClasse"
               	        		);
               	      
               	     //esitiInClassiDS.show();
               	     
               	     esitiInClassiDS.createOrReplaceTempView("gruppoAvvenimenti");
               	  Dataset<Row> classiInAvvenimentoDS = spark.sql("select first(typeMessaggio) as typeMessaggio"
               			  	+ ", first(timestamp) as timestamp"
         	        		+ ", idPalinsesto"
         	        		+ ", idAvvenimento"
         	        		+ ", first(idavvproviderliveAvvenimento) as idavvproviderliveAvvenimento"
         	        		+ ", first(idbetradarliveAvvenimento) as idbetradarliveAvvenimento"
         	        		+ ", first(iddiscAvvenimento) as iddiscAvvenimento"
         	        		+ ", first(idmanifAvvenimento) as idmanifAvvenimento"
         	        		+ ", first(idproviderliveAvvenimento) as idproviderliveAvvenimento"
         	        		+ ", first(legmultiplaAvvenimento) as legmultiplaAvvenimento"
         	        		+ ", first(liveAvvenimento) as liveAvvenimento"
         	        		+ ", first(statoAvvenimento) as statoAvvenimento"
         	        		+ ", first(dataAvvenimento) as dataAvvenimento"
         	        		+ ", first(descrdiscAvvenimento) as descrdiscAvvenimento"
         	        		+ ", first(descrmanifAvvenimento) as descrmanifAvvenimento"
         	        		+ ", first(descrizioneAvvenimento) as descrizioneAvvenimento"
         	        		+ ", first(sigladiscAvvenimento) as sigladiscAvvenimento"
         	        		+ ", first(siglamanifAvvenimento) as siglamanifAvvenimento"
         	        		+ ", collect_list(struct(idClasse, blacklistmaxClasse, blacklistminClasse, descrinfoaggClasse, infoaggClasse, legmaxClasse, legminClasse, liveClasse, livedelayClasse, multiplaClasse, statoClasse, tipoinfoaggClasse, descrizioneClasse, esiti)) as classi"
         	        		+ " from gruppoAvvenimenti"
         	        		+ " group by idPalinsesto, idAvvenimento"
         	        		);
    	          		//classiInAvvenimentoDS.show();
        	        	//write the data into MongoDB if the message/record is not already present
        	        	MongoSpark.save(classiInAvvenimentoDS.write().option("uri", "mongodb://localhost:27017/database.myCollection").mode("overwrite"));    	        		
        	        	//classiavvDS.show();
        	     	}        
       			}       			
             }           
       	});      
       
        jssc.start();
        jssc.awaitTermination();	
    }
    
    
    /** Logger */
    private static void setLogLevels() {
        boolean log4jInitialized = Logger.getRootLogger().getAllAppenders().hasMoreElements();
        if (!log4jInitialized) {
            // We first log something to initialize Spark's default logging, then we override the
            // logging level.
            Logger.getLogger(XMLJobNew.class).info("Setting log level to [WARN] for streaming example." +
                    " To override add a custom log4j.properties to the classpath.");
            Logger.getRootLogger().setLevel(Level.WARN);
        }
    }
    
    
    
    /** Lazily instantiated singleton instance of SparkSession */
    static class JavaSparkSessionSingleton {
    	private static transient SparkSession instance = null;
    	public static SparkSession getInstance(SparkConf sparkConf) {
    		if (instance == null) {
		
    			instance = SparkSession
    					.builder()
    					.config(sparkConf)
    					.config("spark.mongodb.output.uri", "mongodb://127.0.0.1/database.myCollection")
    					//.config("spark.driver.memory", 700m)
    					//.config("spark.executor.memory", 1g)
    					//.config("spark.executor.cores", 1) //perchè ci sono solo 2 core, posso modificare
    					.getOrCreate();
    		}
    		return instance;
    	}

    }
}
