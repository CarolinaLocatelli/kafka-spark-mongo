package com.example.spark;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Palinsesto implements Serializable {

	/**
	 * 
	 */
	static final long serialVersionUID = -4385373718513080822L;
	long id;
	private List<Avvenimento> avvenimenti =new ArrayList<Avvenimento>();
	
	public Palinsesto(){};
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public List<Avvenimento> getAvvenimenti() {
		return avvenimenti;
	}
	public void setAvvenimenti(List<Avvenimento> avvenimenti) {
		this.avvenimenti = avvenimenti;
	} 
	
	
}
