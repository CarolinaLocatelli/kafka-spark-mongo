from lxml import etree
import datetime
from kafka import KafkaClient, SimpleProducer, SimpleConsumer

import sys
reload(sys)
sys.setdefaultencoding('utf8')

# To send messages synchronously
kafka = KafkaClient("localhost:9092")
producer = SimpleProducer(kafka)

tree = etree.parse("/home/corso/Desktop/palinsesti_message_messaggipiccoli2.xml")
root = tree.getroot()

print(root)
start = datetime.datetime.now()
print("INIZIO INGESTIONE")
print(start)

for palinsesti in root:
    message = etree.tostring(palinsesti)
    producer.send_messages("test", message)

end = datetime.datetime.now()
print("FINE INGESTIONE")
print(end)
print (end-start)
