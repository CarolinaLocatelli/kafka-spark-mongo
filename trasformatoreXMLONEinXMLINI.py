import xml.etree.ElementTree as ET
from urllib2 import urlopen
from pymongo import MongoClient
#import datetime
from datetime import datetime
import time
from kafka import KafkaClient, SimpleProducer, SimpleConsumer

import sys
reload(sys)
sys.setdefaultencoding('utf8')

# To send messages synchronously
#kafka = KafkaClient("localhost:9092")
#producer = SimpleProducer(kafka)

# To write to MongoDB
#cl = MongoClient()
#coll = cl["database"]["myCollectionTot"]
#coll.drop()

i= 0

tree = ET.parse("/home/corso/Desktop/palinsesti_message2.xml")

root = tree.getroot()

stringaIntera = ""

start = datetime.now()
print("INIZIO TRASFORMAZIONE")
print(start)

with open("palinsesti_message_messaggipiccoli2.xml", "a") as myfile:
    myfile.write("<?xml version=\"1.0\" encoding=\"UTF-8\" ?><root>")
    
for row in root.findall("palinsesto"):
    
                       
    for row1 in row.findall("avvenimenti"):
        
        for row2 in row1.findall("avvenimento"):
            for row3 in row2.findall("classi"):
                for row4 in row3.findall("classe"):
                     for row5 in row4.findall("esiti"):
                         for row6 in row5.findall("esito"):
                            #creo xml
                            new_tree = ET
                            new_root = new_tree.Element("palinsesti", type=str(root.get("type")))

                            new_tree_tot = ET
			    #creo documento MongoDB: deve contenere avvenimentoId e classeId
                            #doc = {}

                            timestamp = new_tree.SubElement(new_root, "timestamp")
                            timestamp.text = str(i)
                            
                            #aggiungo tag bmnumminesiti
                            for element in root.findall("bmnumminesiti"):
                                bmnnumesiti = new_tree.SubElement(new_root, "bmnumminesiti")
                                bmnnumesiti.text = str(element.text)

                            #aggiungo tag bmpercentuale
                            for element in root.findall("bmpercentuale"):
                                bmpercentuale = new_tree.SubElement(new_root, "bmpercentuale")
                                bmpercentuale.text = str(element.text)

                            #aggiungo tag bmquotaminesiti
                            for element in root.findall("bmquotaminesiti"):
                                bmquotaminesiti = new_tree.SubElement(new_root, "bmquotaminesiti")
                                bmquotaminesiti.text = str(element.text)

                            #aggiungo tag bmssnumminesiti
                            for element in root.findall("bmssnumminesiti"):
                                bmssnumminesiti = new_tree.SubElement(new_root, "bmssnumminesiti")
                                bmssnumminesiti.text = str(element.text)

                            #aggiungo tag bmsspercentuale
                            for element in root.findall("bmsspercentuale"):
                                bmsspercentuale = new_tree.SubElement(new_root, "bmsspercentuale")
                                bmsspercentuale.text = str(element.text)

                            #aggiungo tag bmssquotaminesiti
                            for element in root.findall("bmssquotaminesiti"):
                                bmssquotaminesiti = new_tree.SubElement(new_root, "bmssquotaminesiti")
                                bmssquotaminesiti.text = str(element.text)

                            #aggiungo tag scomimpmin
                            for element in root.findall("scomimpmin"):
                                scomimpmin = new_tree.SubElement(new_root, "scomimpmin")
                                scomimpmin.text = str(element.text)

                            #aggiungo tag scommoltip
                            for element in root.findall("scommoltip"):
                                scommoltip = new_tree.SubElement(new_root, "scommoltip")
                                scommoltip.text = str(element.text)

                            #aggiungo tag scomsoglialeg
                            for element in root.findall("scomsoglialeg"):
                                scomsoglialeg = new_tree.SubElement(new_root, "scomsoglialeg")
                                scomsoglialeg.text = str(element.text)

                            #aggiungo tag scomvinmax
                            for element in root.findall("scomvinmax"):
                                scomvinmax = new_tree.SubElement(new_root, "scomvinmax")
                                scomvinmax.text = str(element.text)

                            #aggiungo tag sogliaaccettazione
                            for element in root.findall("sogliaaccettazione"):
                                sogliaaccettazione = new_tree.SubElement(new_root, "sogliaaccettazione")
                                sogliaaccettazione.text = str(element.text)

                            #aggiungo tag sscomclsdisab
                            for element in root.findall("sscomclsdisab"):
                                sscomclsdisab = new_tree.SubElement(new_root, "sscomclsdisab")
                                sscomclsdisab.text = str(element.text)

                            #aggiungo tag sscomimpminbigl
                            for element in root.findall("sscomimpminbigl"):
                                sscomimpminbigl = new_tree.SubElement(new_root, "sscomimpminbigl")
                                sscomimpminbigl.text = str(element.text)

                            #aggiungo tag sscomimpminunita
                            for element in root.findall("sscomimpminunita"):
                                sscomimpminunita = new_tree.SubElement(new_root, "sscomimpminunita")
                                sscomimpminunita.text = str(element.text)

                            #aggiungo tag sscommaxnumesiti
                            for element in root.findall("sscommaxnumesiti"):
                                sscommaxnumesiti = new_tree.SubElement(new_root, "sscommaxnumesiti")
                                sscommaxnumesiti.text = str(element.text)

                            #aggiungo tag sscommingrp
                            for element in root.findall("sscommingrp"):
                                sscommingrp = new_tree.SubElement(new_root, "sscommingrp")
                                sscommingrp.text = str(element.text)

                            #aggiungo tag sscommoltip
                            for element in root.findall("sscommoltip"):
                                sscommoltip = new_tree.SubElement(new_root, "sscommoltip")
                                sscommoltip.text = str(element.text)

                            #aggiungo tag sscomscadavv
                            for element in root.findall("sscomscadavv"):
                                sscomscadavv = new_tree.SubElement(new_root, "sscomscadavv")
                                sscomscadavv.text = str(element.text)
                            
                            #aggiungo tag palinsesto
                            palinsesto = new_tree.SubElement(new_root, "palinsesto")
                            palinsesto.set("id", str(row.get("id")))

                            #aggiungo tag avvenimenti
                            avvenimenti = new_tree.SubElement(palinsesto, "avvenimenti")

                            ####aggiungo sub-tag avvenimento
                            avvenimento = new_tree.SubElement(avvenimenti, "avvenimento")
                            avvenimento.set("id", str(row2.get("id")))
			    ##scrivo nel doc MongoDB
                            #doc["idAvvenimento"] = str(row2.get("id"))

                            avvenimento.set("stato", str(row2.get("stato")))
                            avvenimento.set("iddisc", str(row2.get("iddisc")))
                            avvenimento.set("idmanif", str(row2.get("idmanif")))
                            avvenimento.set("legmultipla", str(row2.get("legmultipla")))
                            avvenimento.set("live", str(row2.get("live")))
                            avvenimento.set("idavvproviderlive", str(row2.get("idavvproviderlivelive")))
                            avvenimento.set("idbetradar", str(row2.get("idbetradar")))
                            avvenimento.set("idproviderlive", str(row2.get("idproviderlive")))

                            #aggiungo tag data
                            for element in row2.findall("data"):
                                data = new_tree.SubElement(avvenimento, "data")
                                data.text = str(element.text)

                            #aggiungo tag sigladisc
                            for element in row2.findall("sigladisc"):
                                sigladisc = new_tree.SubElement(avvenimento, "sigladisc")
                                sigladisc.text = str(element.text)

                            #aggiungo tag descrdisc
                            for element in row2.findall("descrdisc"):
                                descrdisc = new_tree.SubElement(avvenimento, "descrdisc")
                                descrdisc.text = str(element.text)

                            #aggiungo tag siglamanif
                            for element in row2.findall("siglamanif"):
                                siglamanif = new_tree.SubElement(avvenimento, "siglamanif")
                                siglamanif.text = str(element.text)

                            #aggiungo tag descrmanif
                            for element in row2.findall("descrmanif"):
                                descrmanif = new_tree.SubElement(avvenimento, "descrmanif")
                                descrmanif.text = str(element.text)

                            #aggiungo tag descrizione
                            for element in row2.findall("descrizione"):
                                descrizione = new_tree.SubElement(avvenimento, "descrizione")
                                descrizione.text = str(element.text)
                                
                            

                            classi = new_tree.SubElement(avvenimento, "classi")

                            ########aggiungo tag classe
                            
                            classe = new_tree.SubElement(classi, "classe")
                            classe.set("id", str(row4.get("id")))
                            ##scrivo nel doc MongoDB
                            #doc["idClasse"] = str(row4.get("id"))

                            classe.set("blacklistmax", str(row4.get("blacklistmax")))
                            classe.set("blacklistmin", str(row4.get("blacklistmin")))
                            classe.set("descrinfoagg", str(row4.get("descrinfoagg")))
                            classe.set("infoagg", str(row4.get("infoagg")))
                            classe.set("legmax", str(row4.get("legmax")))
                            classe.set("legmin", str(row4.get("legmin")))
                            classe.set("live", str(row4.get("live")))
                            classe.set("livedelay", str(row4.get("livedelay")))
                            classe.set("multipla", str(row4.get("multipla")))
                            classe.set("stato", str(row4.get("stato")))
                            classe.set("tipoinfoagg", str(row4.get("tipoinfoagg")))

                            for element in row4.findall("descrizione"):
                                descrizione = new_tree.SubElement(classe, "descrizione")
                                descrizione.text = str(element.text)
                            
                            esiti = new_tree.SubElement(classe, "esiti")

                            ########aggiungo tag esito
                                
                            esito = new_tree.SubElement(esiti, "esito")
                            esito.set("id", str(row6.get("id")))
                            esito.set("quota", str(row6.get("quota")))
                            esito.set("stato", str(row6.get("stato")))
                                    

                            for element in row6.findall("descrizione"):
                                descrizione2 = new_tree.SubElement(esito, "descrizione")
                                descrizione2.text = str(element.text)

                            
                            message = ET.tostring(new_root, 'utf-8')
                            i = i+1
                            
                            with open("palinsesti_message_messaggipiccoli2.xml", "a") as myfile:
                                myfile.write(message)
                            #producer.send_messages("test1", message)

                            #coll.insert(doc)
                            #query per vedere valori distinti in mongo e verificare (tot 246642, distinti 37918, ingeriti in 17 min, senza mongo ingeriti in 12 min)
                            #db.myCollectionTot.aggregate([{$group :  {"_id":{"idAvvenimento":"$idAvvenimento", "idClasse":"$idClasse"}, "amt": { $sum: 1 }}},{$count:"distinti"}])




with open("palinsesti_message_messaggipiccoli2.xml", "a") as myfile:
    myfile.write("</root>")

end = datetime.now()
print("FINE TRASFORMAZIONE")
print(end)
print (end-start)
